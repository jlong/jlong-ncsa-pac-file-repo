function FindProxyForURL(url, host) {

    // 7601 - cerberusX.ncsa.illinois.edu
    // 7602 - ache-bastion-X.ncsa.illinois.edu
    // 7603 - mgadm
    // 7604 - mg-adm01
    // 7605 - dt-prov02.delta.internal.ncsa.edu
    // 7606 - hli-adm01.internal.ncsa.edu
    // 7607 - cc-img1.campuscluster.illinois.edu

    // Cerberus
    var cerberus_bastion_proxy = "SOCKS5 127.0.0.1:7601;SOCKS 127.0.0.1:7601";

    if ( host == "set-analytics.ncsa.illinois.edu")
        return cerberus_bastion_proxy;
    if ( host == "vsphere.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgrs.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgrstest.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgrscon.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgrscontest.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgportal1.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgportal.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgportal2.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "mgportaltest.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "viewalert02.techservices.illinois.edu" )
	return cerberus_bastion_proxy;
    if ( host == "openondemand.delta.ncsa.illinois.edu" )
	return cerberus_bastion_proxy;
    var cerberus_bastion_subnets = [
	{
		"name": "DCL Datacenter network for TS Nagios monitoring service",
		"start": "192.17.18.64",
		"netmask": "255.255.255.240"
	},
	{
		"name": "vsphere mgmt net esxi storage ipmi",
		"start": "10.142.192.1",
		"netmask": "255.255.255.0"
	},
        {
                "name": "nebula iDRAC",
                "start": "10.142.208.1",
                "netmask": "255.255.255.0"
        },
        {
                "name": "mg facilities net",
                "start": "172.28.16.0",
                "netmask": "255.255.255.224"
        },
        {
                "name": "Radiant facilities net",
                "start": "172.28.16.32",
                "netmask": "255.255.255.224"
        },
        {
                "name": "ncsa netdot",
                "start": "141.142.141.136",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa netdot-proxy ipam",
                "start": "141.142.141.131",
                "netmask": "255.255.255.255"
        },
        {
                "name": "odcim.ncsa.illinois.edu",
                "start": "141.142.151.10",
                "netmask": "255.255.255.255"
        },
        {       
                "name": "magnus rstudio server",
                "start": "141.142.161.134",
                "netmask": "255.255.255.255"
        },
        {       
                "name": "magnus rstudio connect",
                "start": "141.142.161.135",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa httpproxy",
                "start": "141.142.192.39",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsatest",
                "start": "141.142.193.89",
                "netmask": "255.255.255.255"
        },
//        {
//                "name": "ncsa internal",
//                "start": "141.142.192.200",
//                "netmask": "255.255.255.255"
//        },
        {
                "name": "ncsa internal-test",
                "start": "141.142.192.137",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa avl-test",
                "start": "141.142.192.134",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa edream-test",
                "start": "141.142.193.98",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa rockosocko",
                "start": "141.142.192.206",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa its-foreman",
                "start": "141.142.192.234",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa its-repo",
                "start": "141.142.192.155",
                "netmask": "255.255.255.255"
        },
//        {
//                "name": "ncsa identity",
//                "start": "141.142.193.68",
//                "netmask": "255.255.255.255"
//        },
        {
                "name": "ncsa events",
                "start": "141.142.192.92",
                "netmask": "255.255.255.255"
        },
        {
                "name": "cmdb.ncsa",
                "start": "141.142.192.133",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa cmdb",
                "start": "141.142.192.26",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa cmdb - test",
                "start": "141.142.192.113",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa cmdb - dev kimber7",
                "start": "141.142.192.34",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa mysql",
                "start": "141.142.192.249",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa mysql cluster",
                "start": "141.142.193.144",
                "netmask": "255.255.255.240"
        },
        {
                "name": "ncsa its-monitor",
                "start": "141.142.192.42",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa wiki-test",
                "start": "141.142.192.248",
                "netmask": "255.255.255.255"
        },
        {
                "name": "ncsa jira-test",
                "start": "141.142.192.195",
                "netmask": "255.255.255.255"
        },
        {
                "name": "asd prov",
                "start": "172.28.18.0",
                "netmask": "255.255.255.0"
        },
    ];

    for (var i = 0; i < cerberus_bastion_subnets.length; i++)
    {
	if ( isInNet(host, cerberus_bastion_subnets[i].start, cerberus_bastion_subnets[i].netmask) )
		return cerberus_bastion_proxy;
    }


    // ACHE
    var ache_bastion_proxy = "SOCKS5 127.0.0.1:7602;SOCKS 127.0.0.1:7602";
// 141.142.168.62/26 - VLAN 1912 (ACHE Services Net)
// 192.168.30.102/16 - VLAN 1819 (ipmi)
// 10.156.0.3/29 - VLAN 1910 (mForge DTN RED)
// 10.156.1.19/24 - VLAN 1911 (mForge Interactive GREEN)

    var ache_bastion_subnets = [
        {
                "name": "ngale-ipmi",
                "start": "172.28.28.0",
                "netmask": "255.255.254.0"
        },
        {
                "name": "ache-services-net",
                "start": "141.142.168.0",
                "netmask": "255.255.255.192"
        },
	{
		"name": "mforge-ipmi",
		"start": "172.28.2.0",
		"netmask": "255.255.255.192"
	},
	{
		"name": "mforge-bmc",
		"start": "172.28.14.0",
		"netmask": "255.255.254.0"
	},
	{
		"name": "ache-ipmi",
		"start": "172.28.2.64",
		"netmask": "255.255.255.192"
	},
	{
		"name": "ache-mforge-access",
		"start": "172.28.2.128",
		"netmask": "255.255.255.192"
	},
	{
		"name": "ache-vmware-mgmt",
		"start": "172.28.2.192",
		"netmask": "255.255.255.192"
	},
	{
		"name": "ache-neteng-safetynet",
		"start": "172.28.3.0",
		"netmask": "255.255.255.192"
	},
	{
		"name": "ache-facilities-mgmt",
		"start": "172.28.3.64",
		"netmask": "255.255.255.192"
	},
	{
		"name": "mforge-archive-data",
		"start": "172.30.0.0",
		"netmask": "255.255.255.192"
	},
	{
		"name": "ache-vmware-vmotion",
		"start": "172.30.0.64",
		"netmask": "255.255.255.192"
	},
    ];

    for (var i = 0; i < ache_bastion_subnets.length; i++)
    {
	if ( isInNet(host, ache_bastion_subnets[i].start, ache_bastion_subnets[i].netmask) )
		return ache_bastion_proxy;
    }

    // Magnus
    var mgadm_proxy = "SOCKS5 127.0.0.1:7603;SOCKS 127.0.0.1:7603";
// 172.31.1.0/24 - VLAN ???? (IPMI)

    var mgadm_subnets = [
	{
		"name": "mg-ipmi",
		"start": "172.31.1.0",
		"netmask": "255.255.255.0"
	},
    ];

    for (var i = 0; i < mgadm_subnets.length; i++)
    {
	if ( isInNet(host, mgadm_subnets[i].start, mgadm_subnets[i].netmask) )
		return mgadm_proxy;
    }

    // Magnus (new)
    var mgadm01_proxy = "SOCKS5 127.0.0.1:7604;SOCKS 127.0.0.1:7604";

    var mgadm01_subnets = [
        {
                "name": "mg-ipmi",
                "start": "172.29.15.0",
                "netmask": "255.255.255.0"
        },
        {
                "name": "mg-facilities",
                "start": "172.28.16.160",
                "netmask": "255.255.255.224"
        },
    ];

    for (var i = 0; i < mgadm01_subnets.length; i++)
    {
        if ( isInNet(host, mgadm01_subnets[i].start, mgadm01_subnets[i].netmask) )
                return mgadm01_proxy;
    }

    // Delta
    var delta_proxy = "SOCKS5 127.0.0.1:7605;SOCKS 127.0.0.1:7605";
// 172.28.24.0/23 - VLAN ???? (IPMI)

    var delta_subnets = [
	{
		"name": "delta-ipmi",
		"start": "172.28.24.0",
		"netmask": "255.255.254.0"
	},
    ];

    for (var i = 0; i < delta_subnets.length; i++)
    {
	if ( isInNet(host, delta_subnets[i].start, delta_subnets[i].netmask) )
		return delta_proxy;
    }

    // Holl-I
    var holli_proxy = "SOCKS5 127.0.0.1:7606;SOCKS 127.0.0.1:7606";

    var holli_subnets = [
	{
		"name": "holli-ipmi",
		"start": "172.28.40.0",
		"netmask": "255.255.255.192"
	},
    ];

    for (var i = 0; i < holli_subnets.length; i++)
    {
	if ( isInNet(host, holli_subnets[i].start, holli_subnets[i].netmask) )
		return holli_proxy;
    }

    // ICCP
    var cc_proxy = "SOCKS5 127.0.0.1:7607;SOCKS 127.0.0.1:7607";

    var cc_subnets = [
	{
		"name": "iccp-ipmi-general",
		"start": "10.10.0.0",
		"netmask": "255.255.0.0"
	},
	{
		"name": "iccp-ipmi-golub",
		"start": "10.12.0.0",
		"netmask": "255.255.0.0"
	},
	{
		"name": "iccp-ipmi-10_60_31",
		"start": "10.60.31.0",
		"netmask": "255.255.255.0"
	},
	{
		"name": "iccp-ipmi-10_60_32",
		"start": "10.60.32.0",
		"netmask": "255.255.255.0"
	},
    	{
		"name": "iccp-ipmi-10_60_41",
		"start": "10.60.41.0",
		"netmask": "255.255.255.0"
	},
];

    for (var i = 0; i < cc_subnets.length; i++)
    {
	if ( isInNet(host, cc_subnets[i].start, cc_subnets[i].netmask) )
		return cc_proxy;
    }

    // No match
    
    return "DIRECT";
}
